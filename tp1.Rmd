---
title: |
    | Tutorial 1 
    | NGUYEN Duc Anh - SADR Mohammad
    | Verifié par AKHMIS Yassir
output: 
  pdf_document:
    toc: true
---

# Tutorial 1

# 1. Maximum Likelihood Review

## \*Question 1: Log Likelihood Function

l($\mu$ , $\sigma^2$) = -$\frac{n}{2}$ $\log(2\pi)$ - $\frac{n}{2}$ $\log(\sigma^2)$ - $\frac{1}{2\sigma^2}$ $\sum_{x = 1}^{n} (x_{i}$-$\mu$)\^2

## 1.1 Example of implementation

```{r example_of_implementation}
set.seed(1234)
# create some data
y = rnorm(1000, mean=5, sd=2)
startvals = c(0, 1)
# the log likelihood function
LL = function(mu=startvals[1], sigma=startvals[2], verbose=FALSE) {
ll = -sum(dnorm(y, mean=mu, sd=sigma, log=T))
if (verbose) message(paste(mu, sigma, ll))
ll
}

library(bbmle)
mlnorm = mle2(LL, start=list(mu=2, sigma=1), method="L-BFGS-B", lower=c(sigma=0))
mlnorm
```

## \*Question 2: Log Likelihood Function

```{r }
y_likelihood = -dnorm(y, mean=0, sd=1, log=T)

model = lm(y_likelihood ~ y)

plot(y,y_likelihood)
abline(lm(y_likelihood ~ y))
summary(model)
```

## 1.2 Visualisation

```{r visualisation}
mu = seq(4, 6, length=50); sigma=seq(1.5, 3, length=50)
llsurf = matrix(NA, length(mu), length(sigma))
for (i in 1:length(mu)){
  for (j in 1:length(sigma)){
    llsurf[i,j] = -sum(dnorm(y, mean=mu[i], sd=sigma[j], log=T))
  }
}
rownames(llsurf) = mu
colnames(llsurf) = sigma
library(plot3D)
persp3D(mu,sigma,llsurf,theta=45, phi=30, axes=TRUE,scale=2, box=TRUE, nticks=5,
ticktype="detailed", xlab="", ylab="", zlab="",
main="minus log likelihood")
text3D(5, 1.15, 2100, expression(mu), add = T)
text3D(6.5, 2.25, 2100, expression(sigma^2), add = T)

```

# 2. Linear Model

## 2.1 Simple linear model

## \*Question 3:

### 1 The linear model by considering a Normal Gaussian noise

\begin{center}
$\epsilon \in N(0, \sigma^2)$

$y = \theta_{1}x + \theta_{0} + \epsilon$
\end{center}

### 2 Define the log-likelihood function by considering n observation from previously defined model
\begin{center}

$l(\theta_{0},\theta_{1},\sigma^2) = \log(\frac{1}{\sqrt{2\pi\sigma^2}}) + \sum_{x = 1}^{n} \log(e^{\frac{-(y-(\theta_{1}X + \theta_{0}))^2}{2\sigma^2}})$
\end{center}

```{r }
set.seed(1234)
# predictor
X = rnorm(1000)
# coefficients for intercept and predictor
beta = c(5,2)
# add intercept to X and create y with some noise
y = cbind(1,X)%*%beta + rnorm(1000, sd=2.5)
regLL = function(sigma=1, Int=0, b1=0){
  coefs = c(Int, b1)
  mu = cbind(1,X)%*%coefs
  ll = -sum(dnorm(y, mean=mu, sd=sigma, log=T))
  ll
}

library(bbmle)
mlopt = mle2(regLL, method="L-BFGS-B", lower=c(sigma=0))
summary(mlopt)

```

## *Question 4:
### 1 Run a regression model (using lm() function) and compare to your Maximum Likelihood estimates

```{r }
LM = lm(y~X)
LM$coefficients

summary(LM)
```

### 2 Provide the log-likelihood obtained from your model (hint: logLik() function) and compare to the previous maximum likelihood value obtained previously


```{r }
LM = lm(y~X)
logLik(LM)

-2 * logLik(LM)

```
# 3. Implement a Batch Gradient descent algorithm for the linear model
## 3.1 Simulate some data

```{r }
set.seed(1234)
n <- 100
theta_0=1.3
theta_1=3.4
theta_2=-1.5
x1 <- rnorm(n)
x2 <- rnorm(n)
y <- theta_0+theta_1*x1++theta_2*x2+rnorm(n,sd=4)
Train <- cbind(y,x1,x2)
head(Train)
```

## 3.2 Batch Gradient function
## *Question 5: Implement Batch Gradient descent called batch.GD

```{r }
# X <- Train[,-1]
# y <- matrix(Train[,1],ncol=1)
# theta0 = rep(1,2) 
# b = 0
# alpha <- 0.0001
# epsilon = 0.0001
# batch.GD <- function(theta=theta0,y_true=y,train=X,alpha=alpha,epsilon=epsilon,iter.max){
#   theta_old = theta0
#   total_samples=dim(train)[1] 
#   cost_list = c()
#   epoch_list = c()
#   for(i in 1:iter.max){
#    y_predicted = theta0%*%t(train) + b
#    theta_grad = -(2/total_samples)*t(train)%*%(y_true-t(y_predicted))
#    b_grad = -(2/total_samples)*sum(y_true-t(y_predicted))
#    theta = theta_old - alpha*theta_grad
#    b = b - alpha*theta_grad
#    cost = mean((y_true-t(y_predicted))^2)
#    cost_list = c(cost_list,cost)
#    epoch_list = c(epoch_list,i)
#    if(sqrt(sum(theta_old-theta)^2) < epsilon)
#      {break}
#   }
#   return(c(theta,b,cost, cost_list, epoch_list))
# }
# model=batch.GD(theta=theta0,y_true=y,train=X,alpha=alpha,epsilon=epsilon,iter.max=50)

```


## 3.3 Cost optimum

```{r }
lm.mod <- lm.mod <- lm(y~x1+x2)
theta.hat <- lm.mod$coef
X <-as.matrix(cbind(rep(1,nrow(Train)),Train[,-1]),ncol=ncol(Train)+1)
y <- matrix(Train[,1],ncol=1)
cost <- 0.5*sum((X%*%matrix(theta.hat,ncol=1)-y)**2)
cost


```

## 3.4 Some tests and visualisation

```{r }
# theta0 <-rep(1,3)
# alpha <- 0.0001
# par(mfrow=c(2,2))
# test <- batch.GD(theta=theta0,Train,alpha,epsilon = 0.0000001)
# plot(test$res.cost,ylab="cost function",xlab="iteration",main="alpha=0.0001",type="l")
# abline(h=cost,col="red")
# test <- batch.GD(theta=theta0,Train,alpha=0.005,epsilon = 0.0000001)
# plot(test$res.cost,ylab="cost function",xlab="iteration",main="alpha=0.005",type="l")
# abline(h=cost,col="red")
# test <- batch.GD(theta=theta0,Train,alpha=0.001,epsilon = 0.0000001)
# plot(test$res.cost,ylab="cost function",xlab="iteration",main="alpha=0.001",type="l")
# abline(h=cost,col="red")
# test <- batch.GD(theta=theta0,Train,alpha=0.01,epsilon = 0.0000001)
# plot(test$res.cost,ylab="cost function",xlab="iteration",main="alpha=0.01",type="l")
# abline(h=cost,col="red")

```

## 3.5 Check results with linear model
```{r }
# lm.mod <- lm(y~x1+x2)
# result <- data.frame("lm"=lm.mod$coef,"BGD"=test$theta)
# knitr::kable(result,caption = "Estimates using Linear model and
# Batch Gradient Descent")
```

## *Question 6: Reproduce similar results as Table 1